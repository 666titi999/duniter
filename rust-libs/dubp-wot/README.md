# wot

`dubp-wot` is a crate making "Web of Trust" computations for
the [Duniter] project.

[Duniter]: https://duniter.org/en/

## How to use it

You can add `dubp-wot` as a `cargo` dependency in your Rust project.
